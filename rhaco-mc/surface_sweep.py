import smiles_db as SMILES
import os
import json
import cover

crystal_x = 2.148490
crystal_y = 2.664721
crystal_z = 0.400321

def sweep(m1_dims=[2, 2, 2], density=0.27, method='cuboid'):
    
    molecules = SMILES.dict_compound
    sweep_dict = {}
    m1_unit_cell_area = 5.178310421031491
    n = 0
    for key in molecules:
        mol_dict = {}
        system = cover.gen_system(dims=m1_dims)
        covered_system, coverage_mol_area, surface_area, num_mols = cover.pack_regions(system,
                                                                                    molecules[key],
                                                                                    density=density)

        file_path = "sweep-results/{}-{}-{}den".format(key, method, density)
        os.mkdir(file_path)
        mol_dict['coverage_smiles'] = molecules[key]
        #mol_dict['coverage_mol'] = key
        mol_dict['num_coverage_mols'] = num_mols
        mol_dict['surface_area_ratio'] = coverage_mol_area / surface_area
        mol_dict['unit_cell_area_ratio'] = coverage_mol_area / m1_unit_cell_area
        mol_dict['path'] = file_path
        radius_results_d = {}
        for r in [0.02, 0.04, 0.06, 0.08, 0.10]:
            final_system, mols_remaining = cover.unblock_active_sites_vec(covered_system,
                                                                            radius=r,
                                                                            monolayer=True)
            system_name = '{}-{}rad-{}den-{}.gsd'.format(key, r, density, method)
            final_system.save('{}/{}'.format(file_path, system_name))
            radius_results_d[r] = mols_remaining
            radius_results_d['gsd_file'] = '{}/{}'.format(system_name, file_path)
        mol_dict['radius_results'] = radius_results_d
        sweep_dict['key'] = mol_dict
        n += 1
        if n == 2:
            break

    with open('{}den-{}-results.json'.format(density, method), 'w') as fp:
        json.dump(sweep_dict, fp)
      

if __name__ == "__main__":
    sweep()














