dict_branches = {
        }

dict_constituents = {
        }

dict_backbone = {
        "ptb7": "c5cc4cc3sc(c2c1sccc1cs2)cc3cc4s5",
        "benzodithiophene": "c3cc2cc1sccc1cc2s3",
        "thiophene": "c1csc2c1scc2",
        "ptaa": "N(c1ccccc1)(c2ccccc2)c3ccccc3",
        "pbttt": "c2c(c1sccc1)sc4c2sc(c3sccc3)c4",
        "carbozole": "c1ccc3c(c1)c2ccccc2N3",
        "f8t2": "c1cccc2c1sc3c2cccc3c5ccc(c4sccc4)s5"
        }

dict_compound = {
    "benzene": "c1ccccc1",
    "anthracene": "c1ccc2cc3ccccc3cc2c1",
    "naphthalene": "c1ccc2ccccc2c1",
    "test": "c1sccc1cccccc-c1sccc1cccccc",
    "perylene": "c1cc2cccc3c4cccc5cccc(c(c1)c23)c45",
    "pentacene": "c5ccc4cc3cc2cc1ccccc1cc2cc3cc4c5",
    "phenyleneperylene": "c1ccc2c(c1)c5ccc6cccc7c3cccc4ccc2c(c34)c5c67",
    "pyrene": "c1cc2ccc3cccc4ccc(c1)c2c34",
    "chrysene": "c1ccc2c(c1)ccc3c2ccc4c3cccc4",
    "phenanthrene": "c1ccc2c3ccccc3ccc2c1",
    "benzo(ghi)perylene": "c1cc2ccc3ccc4ccc5cccc6c5c4c3c2c6c1",
    "benzo(a)pyrene": "c1ccc2c(c1)cc3ccc4cccc5c4c3c2cc5",
    "coronene": "c1cc2ccc3ccc4ccc5ccc6ccc1c7c2c3c4c5c67",
    "triphenylene": "c1(cccc3)c3c(cccc4)c4c2c1cccc2",
    "acenaphthene": "c1cc2cccc3c2c(c1)CC3",
    "tetracene": "c34cc2cc1ccccc1cc2cc3cccc4",
    "dibenz(a,h)anthracene": "c1ccc2c(c1)ccc3c2cc4ccc5ccccc5c4c3",
    "corannulene": "c16ccc2ccc3ccc5c4c(c1c2c34)c(cc5)cc6",
    "picene": "c5c3c2ccc1ccccc1c2ccc3c4ccccc4c5",
    "fluoranthene": "c1ccc-2c(c1)-c3cccc4c3c2ccc4"
    }


def compounds(name):
    return dict_compound[name]

def backbone(name):
    return dict_backbone[name]
