import mbuild as mb
import foyer
import numpy as np
from scipy.spatial import ConvexHull

crystal_x = 2.148490
crystal_y = 2.664721
crystal_z = 0.400321


def m1_box(x_dims, y_dims, z_dims, height):
    box_mins = [0, 0, 0]
    box_maxs = [(crystal_x * x_dims),
               (crystal_y * y_dims),
               (crystal_z * z_dims) + height]

    box = mb.box.Box(mins=np.array(box_mins), maxs=np.array(box_maxs))
    return box


def convex_area(compound):
    xy_coords = []
    for coord in compound.xyz:
        point = [coord[0], coord[1]]
        xy_coords.append(point)
    hull = ConvexHull(np.array(xy_coords))
    return hull.volume


def active_cuboid(atom, child, r, z=10):

# https://math.stackexchange.com/questions/1472049/check-if-a-point-is-inside-a-rectangular-shaped-area-3d
    p0 = atom.pos
    p1 = np.array([p0[0]-r, p0[1]-r, p0[2]])
    p2 = np.array([p0[0]-r, p0[1]+r, p0[2]])
    p4 = np.array([p0[0]+r, p0[1]-r, p0[2]])
    p5 = np.array([p0[0]+r, p0[1]+r, p0[2]+z])
    u = p2 - p1
    v = p4 - p1
    w = p5 - p1

    inside = False
    for pos in child.xyz:
        if np.dot(u, p1) <= np.dot(u, pos) <= np.dot(u, p2):
            if np.dot(v, p1) <= np.dot(v, pos) <= np.dot(v, p4):
                if np.dot(w, p1) <= np.dot(w, pos) <= np.dot(w, p5):
                    inside = True
                    break
                else: pass
            else: pass
        else: pass
    return inside


def layers(system, threshold=0.10):
    children_pos = [child.center[2] for child in system.children[1:]]
    children_pos.sort()
    layers = []
    layer = []
    init_value = children_pos[0]
    for i, z in enumerate(children_pos):
        if i == len(children_pos) - 1:
            layers.append(layer.copy())
            break
        if z < init_value + threshold:
            layer.append(z)
        else:
            if len(layer) > 1:
                layers.append(layer.copy())
            init_value = z
            layer.clear()
    layer_heights = [np.amax(z) for z in layers]
    return (layer_heights)


def pack_regions(surface, coverage_mol, num_mols=None,
                  density=1, overlap=0.35,
                  height_buffer=0.0074, edge=0):

    if coverage_mol == None:
        return covered_system
    m1_box = surface.boundingbox
    layer_box = surface.boundingbox
    layer_box.maxs[-1] += height_buffer
    layer_box.mins[-1] = m1_box.maxs[-1]

    # Create mb compound used to cover surface:
    if isinstance(coverage_mol, str):
        if len(coverage_mol.split('.')) > 1: # loading from a file
            coverage_comp = mb.load(coverage_mol)
        else: # loading from smiles string
            coverage_comp = mb.load(coverage_mol, smiles=True)
    elif isinstance(coverage_mol, mb.compound.Compound):
        coverage_comp = coverage_mol  # using an existing mb compound

    if not num_mols: # Find relative sizes of coverage_mol and surface
        coverage_mol_area = convex_area(coverage_comp)
        surface_area = convex_area(surface)
        num_mols = int(round((surface_area // coverage_mol_area) * density, 0))

    covered_system = mb.fill_region(compound = [surface, coverage_comp],
                                   n_compounds = [1, num_mols],
                                   region = [m1_box, layer_box],
                                   fix_orientation=[(True,True, True),(True, False, True)],
                                   overlap=overlap,
                                   edge=edge
                                   )
    covered_system.Box = surface.Box

    return covered_system


def unblock_sites(surface, atoms_to_unblock=['Mo', 'V', 'Te', 'Nb'],
                  radius=0.0666666, zmax_cut=0.115, monolayer=False, layer_threshold=0.10):

    oxygen_atoms = [p for p in surface.particles() if p.name == 'O']
    active_site_atoms = [p for p in surface.particles() if p.name in atoms_to_unblock]
    active_atom_zpos = np.array([p.pos[-1] for p in active_site_atoms])
    oxygen_atom_zpos = np.array([p.pos[-1] for p in oxygen_atoms])
    zmax = np.amax(active_atom_zpos)
    
    active_sites = [] # append particles here
    for p in active_site_atoms:
        if p.pos[-1] > zmax - zmax_cut: # is on the top layer of M1
            bonded_atoms = [b for b in surface.bonds() if b[0] == p or b[1] == p]
            if len(bonded_atoms) == 1: # p is one of the "floating" active site atoms
                for bond in bonded_atoms:
                    if bond[0].name != p.name:
                        if bond[0].pos[-1] < p.pos[-1]: # atom is above the oxygen it is bonded to
                            active_sites.append(p)
                    elif bond[1].name != p.name:
                        if bond[1].pos[-1] < p.pos[-1]: # atom is above the oxygen it is bonded to
                            active_sites.append(p)
    if monolayer:
        layer_heights = layers(surface, threshold=layer_threshold)

    for child in surface.children[1:]:
        if child.center[-1] < np.amax(oxygen_atom_zpos): # remove compounds that get
            surface.remove(child)                         # placed inside of M1 system
            pass
        if monolayer:
            if child.center[-1] > layer_heights[0]:  # remove compounds with z values
                surface.remove(child)                 # larger than the height of first organic layer
                pass
        for atom in active_sites:
            inside = active_cuboid(atom, child, radius)
            if inside:
                surface.remove(child)  # remove compound if any atom within it is covering active sites
    return surface
