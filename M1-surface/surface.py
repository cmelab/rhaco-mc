import mbuild



# Environment Stuff:
'''
mbuild = 0.10.4
conda install -c conda-forge openbabel  --> Required for mb compounds
conda install -c conda-forge py3Dmol --> Required to visualize mb compounds in jupyter notebookes

'''


# Notes from tutorial:
'''
Compounds can be created by generating and connecting particles one-by-one; however,
it is typically more practical to load coordinates and bonds from a structure file (such as PDB, MOL2, etc.)
or to import `Compound` class definitions that other users have already defined.



'''
